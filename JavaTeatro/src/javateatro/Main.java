/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javateatro;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Genre fantasy = new Genre();
        fantasy.setName("Fantasy");
        
        Location blaBlaStreat_11_54 = new Location();
        blaBlaStreat_11_54.setAddress("Bla-bla street");
        
        Place place = new Place();
        place.setRow(11);
        place.setNumber(54);
        blaBlaStreat_11_54.setPlace(place);
        
        Price normalPriceUAH = new Price();
        normalPriceUAH.setLevel("normal");
        normalPriceUAH.setCost(600.);
        normalPriceUAH.setCurrency("UAH");
        
        Performance p1 = new Performance();
        p1.setGenre(fantasy);
        p1.setLocation(blaBlaStreat_11_54);
        p1.setPrice(normalPriceUAH);
        
        System.out.printf("%s [%s%s]\n", p1.getGenre().getName(), p1.getPrice().getCost(), p1.getPrice().getCurrency());
        System.out.println(p1);
//        System.out.println(p1.toString());
    }
    
}
