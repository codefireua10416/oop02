/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire;

import java.util.Calendar;
import java.util.Locale;

/**
 *
 * @author human
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        
        calendar.set(Calendar.YEAR, 2016);
        calendar.set(Calendar.MONTH, 01); // Start from 0-11
        calendar.set(Calendar.DAY_OF_MONTH, 16);
        
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.YEAR, 2015);
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.YEAR, 2014);
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        calendar.set(Calendar.YEAR, 2013);
        System.out.println(calendar.get(Calendar.DAY_OF_WEEK));
        
        calendar.set(Calendar.YEAR, 2016);
        System.out.println(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_FORMAT, Locale.getDefault()));
        calendar.set(Calendar.YEAR, 2015);
        System.out.println(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_FORMAT, Locale.getDefault()));
        calendar.set(Calendar.YEAR, 2014);
        System.out.println(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_FORMAT, Locale.getDefault()));
        calendar.set(Calendar.YEAR, 2013);
        System.out.println(calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG_FORMAT, Locale.getDefault()));
    }
    
}
